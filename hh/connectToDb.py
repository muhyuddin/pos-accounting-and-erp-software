import pymysql.cursors
from hh.UTIL import DB

# Connect to the database
def connectToDB():
    # lines = [line.rstrip('\n') for line in open(os.   'data/db.txt')]
    # connection = pymysql.connect(host=lines[0],
    #                              user=lines[2],
    #                              password=lines[3],
    #                              db=lines[4],
    #                              charset=lines[5],
    #                              cursorclass=pymysql.cursors.DictCursor)

    connection = pymysql.connect(host=DB.hostname,
                                 user=DB.username,
                                 password=DB.password,
                                 db=DB.db_name,
                                 charset=DB.encode_type,
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection
    
# 
